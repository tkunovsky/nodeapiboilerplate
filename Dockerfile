FROM node:10.16.0-alpine

WORKDIR /usr/nodeApi

COPY package.json .

RUN npm install

COPY . .

RUN npm run build

EXPOSE 8080

CMD [ "node", "dist/" ]
